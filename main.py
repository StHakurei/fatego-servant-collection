"""FateGO-Servant-Collection: Program Entry.

This program will automatically query latest servant info and download servant's avatat and graph
from GrandOrder.wiki, all images will be stored in Ouput folder and sorted in servant id.
"""

__author__ = "Langston.Hakurei"
__version__ = "1.0"
__status__ = "Production"

import os
import shutil
import requests
import argparse
from bs4 import BeautifulSoup
from progress.bar import ShadyBar

# Define wiki page URL for each game region.
JP_URL = "https://grandorder.wiki/Servant_List"
EN_URL = "https://grandorder.wiki/Servant_List/EN"

# Bypass emeney or unsummonable characters.
EXCEPTION_OBJECT = ["149", "151", "152", "168", "083"]


class ServantCollection(object):
    """Service class.

    Note:
        Servant collection service.
    """

    def __init__(self, region):
        """Initialize request info.

        Note:
            Set HTTP request headers.
            Pre-define output directories.

        Args:
            region(str): FGO game region. EN/JP.
        """
        self.region = region
        self.headers = {
            "user-agent": "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
        }
        if region == "EN":
            self.url = EN_URL
        else:
            self.url = JP_URL

        self.avatar_path = "Output/" + self.region + "/avatar/"
        self.graph_path = "Output/" + self.region + "/graph/"

        self.graph_missing = 0

    def update(self, dry_run, verbose):
        """Retrieve servant info from GrandWiki.

        Note:
            Retrieve wiki page and find servant information.

        Args:
            dry_run (bool): Retrieve servant info only.
            verbose (bool): Output detail.
        """
        # Detect Output folder
        if not os.path.exists("Output"):
            try:
                os.makedirs("Output")
                os.makedirs("Output/EN")
                os.makedirs("Output/JP")
            except OSError:
                print(
                    "Failed to create Output directory, please check your permissions."
                )
                exit(0)

        # Retrieve data from GrandOrder wiki.
        print(f"Set FGO region: {self.region}")
        response = requests.get(self.url, self.headers)

        if response.status_code == 200:

            print("Get servants info: Ok")
            html_page = BeautifulSoup(response.content, "html.parser")
            servant_table = html_page.find("table", class_="sortable").find_all("tr")
            index = 1
            folder_number = len(servant_table) - 1
            print("Found servant record: " + str(folder_number - len(EXCEPTION_OBJECT)))
            if not dry_run or verbose:
                bar = ShadyBar("Download: ", max=folder_number - len(EXCEPTION_OBJECT))

            while index < len(servant_table):
                servant = servant_table[index].find_all("td")
                servant_name = str(servant[1].find("a")["title"])
                servant_id = str(servant[0].get_text()[0:3])
                avatar_link = servant[1].find("img")["src"]
                graph_page = servant[1].find("a")["href"]
                rarity = servant[4].get_text()[0:2]

                if not dry_run:
                    if servant_id not in EXCEPTION_OBJECT:
                        if verbose:
                            print(f'Get: [{servant_id}] "{servant_name}" - {rarity}')

                        self.save_avatar(
                            avatar_link, folder_number, servant_name, verbose
                        )
                        self.save_graph(
                            graph_page, servant_id, folder_number, servant_name, verbose
                        )

                        if not verbose:
                            bar.next()
                else:
                    print(f'Get: [{servant_id}] "{servant_name}" - {rarity}')

                index = index + 1
                folder_number = folder_number - 1

            if not dry_run or verbose:
                bar.finish()

            if self.graph_missing > 0:
                print(
                    "Some servant's saint graphs may fail to download, please try again later, the program will automatically skip the existed saint graph."
                )
        else:
            print(
                "Unable to request data from GrandOrder Wiki. Please check your internet connection."
            )
            exit(0)

    def save_avatar(self, image_url, folder, name, verbose):
        """Download Avatar.

        Note:
            Download servant's avatar image.

        Args:
            image_url(str): Servant Avatar image download url.
            folder(str): Image store folder.
            name(str): Servant name.
            verbose(bool): print download message.
        """
        avatar_url = "https://grandorder.wiki" + image_url
        # Skip id 240.
        if folder > 239:
            folder = folder + 1
        store_path = self.avatar_path + str(folder) + "/1.png"

        if not os.path.exists(store_path):
            if not os.path.exists(self.avatar_path + str(folder)):
                try:
                    os.makedirs(self.avatar_path + str(folder))
                except OSError:
                    print(
                        "Failed to create servant folder, please check your permissions."
                    )
                    exit(0)
            self.download_image(store_path, avatar_url)
            if verbose:
                print("Download Avatar - Ok.")
        else:
            if verbose:
                print("Avatar exists, skip.")

    def save_graph(self, url, id, folder, name, verbose):
        """Download Saint Graph.

        Note:
           Retrieve graph url from servant information page.

        Args:
            url(str): Servant GrandOrder wiki page.
            id(str): Servant's in_game id.
            folder(str): Image store folder.
            name(str): Servant name.
            verbose(bool): print download message.
        """
        graph_url = "https://grandorder.wiki" + url + "#4th"
        # Skip id 240.
        if folder > 239:
            folder = folder + 1
        store_path = self.graph_path + str(folder) + "/4.png"

        if not os.path.exists(store_path):
            response = requests.get(graph_url, self.headers)
            if response.status_code == 200:
                graph_page = BeautifulSoup(response.content, "html.parser")
                graph4_link = (
                    "https://grandorder.wiki"
                    + graph_page.find(
                        attrs={"alt": "Portrait Servant " + id + " 4.png"}
                    )["src"]
                )

                if not os.path.exists(self.graph_path + str(folder)):
                    try:
                        os.makedirs(self.graph_path + str(folder))
                    except OSError:
                        print(
                            "Failed to create servant folder, please check your permissions."
                        )
                        exit(0)

                self.download_image(store_path, graph4_link)
                if verbose:
                    print("Download Saint Graph - Ok.\n")
            else:
                print("Download Saint Graph - Failed.\n")
                self.graph_missing = self.graph_missing + 1
        else:
            if verbose:
                print("Saint Graph exists, skip.\n")

    def download_image(self, path, url):
        """Image download function.

        Args:
            path(str): Image store path.
            url(str): Image download url.
        """
        image = requests.get(url, stream=True)
        with open(path, "wb") as file:
            for chunk in image.iter_content(chunk_size=1024):
                if chunk:
                    file.write(chunk)

    def clear_output(self):
        """Clear function.

        Note:
            Remove Output folder and all stored images.
        """
        if os.path.exists("Output"):
            try:
                shutil.rmtree("Output")
            except IOError:
                print("Failed to clear Output folder, please check your permissions.")


# FateGO collection CLI.
params = argparse.ArgumentParser(
    description=f"FateGO Servant Collection (v{__version__})"
)
params.add_argument(
    "--region", default="EN", help="Set game region, default=EN", choices=["EN", "JP"]
)
params.add_argument("--clear", action="store_true", help="Clear Output folder")
params.add_argument(
    "--dry_run",
    action="store_true",
    help="Retrieve servant record without download image",
)
params.add_argument("-v", "--version", action="store_true", help="Show program version")
params.add_argument(
    "-V", "--verbose", action="store_true", help="Output download detail"
)

args = params.parse_args()
fatego_cli = ServantCollection(args.region)

if __name__ == "__main__":
    try:
        # Execute program.
        if args.clear:
            fatego_cli.clear_output()
        elif args.dry_run:
            fatego_cli.update(args.dry_run, False)
        elif args.version:
            print(f"FateGO Servant Collection (v{__version__})")
        else:
            fatego_cli.update(args.dry_run, args.verbose)
    except KeyboardInterrupt:
        print("\nQuit...")
        exit(0)
