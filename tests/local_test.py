from main import ServantCollection

__author__ = "Langston.Hakurei"
__version__ = "1.0"
__status__ = "Production"


def test_dryrun_en():
    try:
        object = ServantCollection("EN")
        object.update(True, False)
        assert True
    except:
        assert False


def test_dryrun_jp():
    try:
        object = ServantCollection("JP")
        object.update(True, False)
        assert True
    except:
        assert False


def test_avatar_download():
    try:
        object = ServantCollection("EN")
        object.save_avatar(
            "/images/thumb/7/75/Icon_Servant_001.png/70px-Icon_Servant_001.png",
            "1",
            "Mash Kyrielight",
            False,
        )
        assert True
    except:
        assert False


def test_graph_download():
    try:
        object = ServantCollection("JP")
        object.save_graph("/Mash_Kyrielight", "001", "1", "Mash Kyrielight", False)
        assert True
    except:
        assert False


def test_clear_output():
    try:
        object = ServantCollection("EN")
        object.clear_output()
        assert True
    except:
        assert False
