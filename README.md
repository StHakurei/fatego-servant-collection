<div align="center">
    <img src="FateGO_logo.png"/>
</div>

[![pipeline status](https://gitlab.com/StHakurei/fatego-servant-collection/badges/master/pipeline.svg)](https://gitlab.com/StHakurei/fatego-servant-collection/pipelines)
[![coverage report](https://gitlab.com/StHakurei/fatego-servant-collection/badges/master/coverage.svg)](https://gitlab.com/StHakurei/fatego-servant-collection/-/commits/master)

[[_TOC_]]


# Project Info

FateGO Servant Collection is a CLI program that could help you retrieve servant avatars and graphs from [GrandOrder Wiki](https://grandorder.wiki/)
and store them to your local directory and for further use. This script currently supports Fate/GrandOrder JP/US region.

## Dependencies

HTTP library: [Requests](https://requests.readthedocs.io/en/master/)

HTML Parser: [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

`pip3 -r requirements.txt`

## Output Folder Structure
```bash
Output/
├── EN
│   ├── avatar
│   │   ├── 219
│   │   │   └── 1.png
│   │   ├── 220
│   │   │   └── 1.png
│   │   .
│   │   .
│   │  
│   └── graph
│       ├── 219
│       │   └── 4.png
│       ├── 220
│       │   └── 4.png
│       .
│       .
│      
└── JP
    ├── avatar
    │   ├── 287
    │   │   └── 1.png
    │   ├── 288
    │   │   └── 1.png
    │   .
    │   .
    │  
    └── graph
        ├── 287
        │   └── 4.png
        ├── 288
        │   └── 4.png
        .
        .

```

## Usage

Quick start. Default region is EN.

`python3 main.py`

Set game region. [EN/JP]

`python3 main.py --region JP`

Dryrun, get servants information but don't download images.

`python3 main.py --dry_run`

Output download progress detail.

`python3 main.py --verbose`

Clear download folder.

`python3 main.py --clear`

Get help and other usage.

`python3 main.py -h`

## Troubleshooting

Sometimes image download may failed due to network connection issue or hit the remote server request limit, just re:run the command,
it will skip avatars and graphs which are already exists.

## License
[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
